#!/usr/bin/env python3
from exapi import exapi


def get_markets(exchange_name):
    ex = exapi.exs[exchange_name]['PUBLIC']
    try:
        markets = ex.get_markets()
    except:
        markets = {}
    return markets


def get_orderbook(exchange_name, base=None, quote=None, limit=None, side=None):
    ex = exapi.exs[exchange_name]['PUBLIC']
    return ex.get_orderbook(base, quote, limit, side)


def get_history(exchange_name, base=None, quote=None, limit=None, since=None):
    ex = exapi.exs[exchange_name]['PUBLIC']
    return ex.get_history(base, quote, limit, since)
    

def get_details(exchange_name, base=None, quote=None):
    ex = exapi.exs[exchange_name]['PUBLIC']
    return ex.get_details(base, quote)


def get_candles(exchange_name, base=None, quote=None, interval='1h', since=None, limit=None):
    ex = exapi.exs[exchange_name]['PUBLIC']
    # not every class has get_candles defined
    if hasattr(ex, 'get_candles') and callable(getattr(ex, 'get_candles')):
        return ex.get_candles(base, quote, interval, since, limit).to_json()
    return []
