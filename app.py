from flask import Flask
from flask_restful import Resource, Api

from price_cacher import PriceCacher, PCError


app = Flask(__name__)
api = Api(app)

cacher = PriceCacher()


class PriceView(Resource):
    def get(self, exchange, base, quote):
        try:
            return {
                'success': True,
                'price_str': str(cacher.get_price(exchange, base, quote)),
                'price_float': float(cacher.get_price(exchange, base, quote)),
            }
        except PCError as e:
            return {
                'success': False,
                'error': str(e),
            }

class Healthcheck(Resource):
    def get(self):
        return {'message': 'price-cacher is running'}

api.add_resource(PriceView, '/<string:exchange>/<string:base>/<string:quote>')
api.add_resource(Healthcheck, '/health')

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
