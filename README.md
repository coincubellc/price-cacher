<h1>price-cacher</h1>
This repo contains the Coincube Price Cacher Module.

<h2>Getting Started</h2>
In order to get up and running with this repo:

<h2>Submodules</h2>
This repo contains one primary submodule and two nested submodules (inside of `exapi` there are two submodules, `models` and `crypto-balances`) which all need to be pulled down before you will be able to work.<br>

<h3>In order to pull in the `exapi` submodule you will need to cd into /price-cacher and run:</h3>
`git submodule init`<br>
`git submodule update`<br>

<h3>In order to pull in the submodules nested inside of `exapi` you will need to cd into /price-cacher/exapi and run:</h3>
`git submodule init`<br>
`git submodule update`<br>

<h3>Please be sure to push submodule changes to remote</h3>
<h5><a href="https://git-scm.com/book/en/v2/Git-Tools-Submodules">Git Submodule Tutorial #1</a></h5>
<h5><a href="https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial">Git Submodule Tutorial #2</a></h5>

<h2>Docker Setup</h2>
You will need <a href="https://docker.com" target="_blank">Docker</a>.

Build the Docker container(s):
For local environment: `docker-compose build`<br>
For production environment: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml build`<br>

Run the Docker container(s):
For local environment: `docker-compose up`<br>
For production environment: `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

<h3>Shell into a specific container</h3>
Find "CONTAINER ID": `docker ps`<br>

Shell into container: `docker exec -it "CONTAINER ID" bash`<br>
(i.e. `docker exec -it 78e539ca25be bash`)

<h3>View container logs</h3>
`docker logs -f "CONTAINER ID"`

<h3>Test the API</h3>
In your browser:
`http://0.0.0.0:5000/exchange/base/quote`

For example:
`http://0.0.0.0:5000/Bitfinex/ETH/BTC`