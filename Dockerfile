FROM continuumio/anaconda3:5.0.1

# Python packages
RUN conda install -c bioconda mysqlclient
RUN conda install -c conda-forge passlib flask-login flask-wtf flask-mail flask-user 

ADD requirements.txt /price-cacher/requirements.txt
RUN pip install -r /price-cacher/requirements.txt

ADD . /price-cacher
WORKDIR /price-cacher

EXPOSE 5000